# AtlassianJiraOnEC2
This repository is to deploy Atlassian Jira on AWS EC2 instance with Automated script using Shell, AWS cloudFormation and Ansible

# Assumptions:

	This automated script only works on RHEL-7 linux version only. As AMI is hardcoded and java8 installation which is prerequisite for Jira to run is targeted only for RHEL systems.

# Pre-requisites:

1. Install aws cli on your system.
2. Configure AWS CLI.
3. Make sure, the credentials you are using have permission boundaries set to create stack and access EC2 instaces.
4. Install Ansible 2.6 and Above

# Limitations:
1. This project is developed on Linux ubuntu 18.08 and all the instructions holds good only for Linux machines.
2. AMI ID, instance type and Key  are hardcoded in the cloud formation template, if needed they can be made user inputs.
3. Ansible Configuration is manual, we need to edit hosts and ansible.cfg file before running Ansible playbooks to install Jira server. (NOTE : This can be automated by reading the public IP of the EC2 instance using AWS cli commands and run Ansible playbooks)
 

# Steps to run:

1. Clone repo:
	* git clone https://amrithprabhu@bitbucket.org/amrithprabhu/atlassianexcersice.git
2. Create key value pair named : 
	* provEC2
3. Make shell script executable(Create stack on AWS, with EC2, SG, VPC):
	* chmod +x aws_create_stack.sh
4. Run the shell script
	* ./aws_create_stack.sh
5. Once EC2 instances are up and running, copy public IPv4 address and configure Ansible to use it as remote host.

# Ansible Configuration :
1. Open File: vim /etc/ansible/ansible.cfg
	* Add these below params
	     * remote_user = ec2-user
		 * private_key_file = /home/amrith/work/AmrithAtlasian/provEC2.pem
	* Open File: vim /etc/ansible/hosts and Add these below params
		 * [ec2servers]
		 * #X.X.X.X   --- Public IP of EC2 instance
3. Run Ansible playbook to install Java8 and Jira on Ec2 instance:
4. Goto : atlassianexcersice/Ansible-playbooks
	* ansbible-playbook -vvvv jira.yml
